
const ProduitList = require('../models/ProductList');


module.exports = {


    getAll :function (req, res) {

         res.json(ProduitList)
    },

    getById :function (req, res) {
        console.log(typeof(req.params.id))
        var current = null ;
        if(req.params.id === null){
            return  res.json(null)
        }

      for(var i = 0 ; i< ProduitList.length ; i++){

          if(req.params.id === ProduitList[i].id.toString()){

           current = ProduitList[i]
              break ;
          }
      }
        return  res.json(current)
    },

    getPhoto : function (req , res) {

        res.sendFile(__dirname+'/uploads/'+ req.params.image)


    }






};
