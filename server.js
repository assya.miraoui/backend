require('dotenv').config(); // Sets up dotenv as soon as our application starts

const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const produitRouter  = require ('./router/produitRouter');

const app = express();
const router = express.Router();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors('*'))
app.use('/api', (req, res, next) => {
    res.send('Hello Word');
    next();
});



app.use('/produit', produitRouter);




app.listen(process.env.PORT, () => {
    console.log(`Server now listening at localhost:${process.env.PORT}`);
});

module.exports = app;