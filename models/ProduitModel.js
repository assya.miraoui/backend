const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const ProduitSchema = new Schema({
    NomProduit: {
        type: String,
        trim: true,
    },
    Nom: {
        type: String,
        trim: true,
    },
    image: {
        type: String,
        trim: true,
    },
    prix: {
        type: String,
        trim: true,
    },


});

module.exports = mongoose.model('Prdouit', ProduitSchema);