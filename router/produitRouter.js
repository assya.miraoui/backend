const produitCtr = require('../controllers/ProduitController');

const multer=require ('multer');
const upload = multer({dest:__dirname+'/uploads/images'});
const router=require('express').Router();

router.get('/getPhoto/:image',produitCtr.getPhoto);
router.get('/',produitCtr.getAll);
router.get('/:id',produitCtr.getById);



module.exports = router ;